Attribute VB_Name = "lofting_circles"
' tutorials:
'     lofting = https://forums.autodesk.com/t5/inventor-forum/straight-loft-transition-how-to/td-p/8734343
' coordinates seem to be mesaured from the bottom left corner as 0, 0, 0
' Make sure the top is work plane 1 and the bottom is work plane 2
Sub runPlaceCirlces()

    ' /10 after everything owing to system not recognising mm values in oUOM properly - known issue when I do it, haven't looked for solution yet.
    ' CHANGES: adjust the following values to change where everything goes
    circleDiameterTop = 4.3 / 10
    circleDiameterBottom = 9.9 / 10
    circleSpacingTop = 4.9 / 10
    CircleSpacingBottom = 10.5 / 10
    topEdgeLength = 78.4 / 10
    bottomEdgeLength = 168 / 10
    
    ' values to control how far away from 0,0 (centre of plane) the objects will start - originally set to radius, but leaves them on the mirror line.
    ' Defintion is that it should be (Spacing - 2*radius / 2) + (radius) - e.g. 4.9 - 4.3 / 2 = 0.3
    addtional_x_spacing_top = ((circleSpacingTop - circleDiameterTop) / 2) + (circleDiameterTop / 2)
    addtional_x_spacing_bot = ((CircleSpacingBottom - circleDiameterBottom) / 2) + (circleDiameterBottom / 2)
    addtional_z_spacing_top = ((circleSpacingTop - circleDiameterTop) / 2) + (circleDiameterTop / 2)
    addtional_z_spacing_bot = ((CircleSpacingBottom - circleDiameterBottom) / 2) + (circleDiameterBottom / 2)


    Dim partDoc As PartDocument
    Set partDoc = ThisApplication.ActiveDocument
    
    Dim partDef As PartComponentDefinition
    Set partDef = partDoc.ComponentDefinition
    
    Dim oTG As TransientGeometry
    Set oTG = ThisApplication.TransientGeometry
    
    Dim oUOM As UnitsOfMeasure
    Set oUOM = partDoc.UnitsOfMeasure
    Dim typeEnum As UnitsTypeEnum
    typeEnum = oUOM.LengthUnits
    'oUOM_1 =
    If oUOM.LengthUnits <> UnitsTypeEnum.kMillimeterLengthUnits Then
        Debug.Print "Problem with Units. Found " & oUOM.GetStringFromType(oUOM.LengthUnits) & " instead of millimeter"
        oUOM.LengthUnits = UnitsTypeEnum.kMillimeterLengthUnits
    End If
    
    Dim workPlaneTop As workPlane
    Dim workPlaneBottom As workPlane

    Dim objType As ObjectTypeEnum
    objType = partDef.WorkPlanes.item(4).Type
    
    Dim sketch As PlanarSketch
    Set workPlaneTop = partDef.WorkPlanes.item(4)
    Set workPlaneBottom = partDef.WorkPlanes.item(5)
    Dim loftDef As LoftDefinition
    Dim loftFeature As loftFeature
    item_num = partDef.Sketches.Count
    
    
    currentName = partDef.Sketches.item(item_num).Name
    currentCount = 0
    ' pre define the coordinates to give assign memory and to force type
    Dim x_top As Double
    Dim x_bottom As Double
    Dim z_top As Double
    Dim z_bottom As Double
    
    Dim sk As sketch
    Dim top As sketch
    Dim bot As sketch
    Dim NumTop
    Dim NumBot
    currentCount = 0

    
    For x_direction = 0 To 7
        For z_direction = 0 To 7
            x_top = x_direction * circleSpacingTop + (circleDiameterTop / 2)    ' The number of places * the distance between places + 1 radius to get into the middle
            x_bottom = (x_direction * CircleSpacingBottom + (circleDiameterBottom / 2))
            z_top = (z_direction * circleSpacingTop + (circleDiameterTop / 2))   ' The number of places * the distance between places + 1 radius to get into the middle
            z_bottom = (z_direction * CircleSpacingBottom + (circleDiameterBottom / 2))
            
            x_top = x_direction * circleSpacingTop + addtional_x_spacing_top    ' The number of places * the distance between places + 1 radius to get into the middle
            x_bottom = (x_direction * CircleSpacingBottom + addtional_x_spacing_bot)
            z_top = (z_direction * circleSpacingTop + addtional_z_spacing_top)   ' The number of places * the distance between places + 1 radius to get into the middle
            z_bottom = (z_direction * CircleSpacingBottom + addtional_z_spacing_bot)
            
            Dim oUOM1 As UnitsOfMeasure
            Set oUOM1 = partDoc.UnitsOfMeasure
            
            'Dim oCol As ObjectCollection
            'Set oCol = ThisApplication.TransientObjects.CreateObjectCollection()
        
            'Set sketch = partDef.Sketches.Add(workPlaneTop)
            'Set sketch = partDef.Sketches.Add(partDef.SurfaceBodies.item(1).Faces.item(6))
            Set sketch = partDef.Sketches.Add(workPlaneTop)
            sketch.Name = "Top" & currentCount
            Call drawCircle(z_top, x_top, circleDiameterTop / 2, sketch, oTG)
            
            
            Set sketch = partDef.Sketches.Add(workPlaneBottom)
            sketch.Name = "Bottom" & currentCount
            Call drawCircle(z_bottom, x_bottom, circleDiameterBottom / 2, sketch, oTG)
            
            
            currentCount = currentCount + 1
        Next z_direction
        breakx = 0
    Next x_direction
    
    currentCount = 0
    
    For Each sk In partDef.Sketches
        If InStr(sk.Name, "Top") Then
            NumTop = Mid(sk.Name, 4, Len(sk.Name) - 1)
            'Debug.Print sk.Name
            'Debug.Print "num = " & NumTop
            Set top = sk
        End If
        If InStr(sk.Name, "Bottom") Then
            NumBot = Mid(sk.Name, 7, Len(sk.Name) - 1)
            Set bot = sk
        End If
        If NumBot = NumTop And NumBot <> "" Then
            'Debug.Print "Num Top = Num Bot for num " & NumTop
            Dim oCol As ObjectCollection
            Set oCol = ThisApplication.TransientObjects.CreateObjectCollection()
            Call oCol.Add(top.Profiles.AddForSolid)
            Call oCol.Add(bot.Profiles.AddForSolid)
            
            Set loftDef = partDef.Features.LoftFeatures.CreateLoftDefinition(oCol, PartFeatureOperationEnum.kCutOperation) ' loft definition: create loft in collection, and make it a surface operation.
            Set loftFeature = partDef.Features.LoftFeatures.Add(loftDef)
            loftFeature.Name = "LoftCoded" & currentCount
            currentCount = currentCount + 1
        End If
        
        
    Next sk

    
End Sub

Sub drawCircle(x As Double, y As Double, radius As Double, sketch As PlanarSketch, tg As TransientGeometry)

    Dim circ As SketchCircle
    Set circ = sketch.SketchCircles.AddByCenterRadius(tg.CreatePoint2d(x, y), radius)
    breax = 0
End Sub
